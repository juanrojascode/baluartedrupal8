(function ($) {
    
    $(document).ready(function(){
        /**
         * On click for carousels
         */
        let valBol = true;
        $('.pause').on('click', function () {
            let carousel = $(this).parents('.carousel');
            console.log(carousel);
            if (valBol){
                $(this).find('i').removeClass('fa-pause').addClass('fa-play');
                carousel.carousel('pause');
                valBol = false;
            }else{
                $(this).find('i').removeClass('fa-play').addClass('fa-pause');
                carousel.carousel('cycle');
                valBol = true;
            }
        });
        /*
         * traducir "search"
         */
        $('#block-theme-baluarte-search input').attr('placeholder', 'Buscar...');

        /*
         * Acción menu hamburguesa
         */
        $('#block-menuhamburguesa').on('click', function () {
           $('#block-theme-baluarte-main-menu').addClass('show');
        });
        $('#block-theme-baluarte-main-menu .nav-item:first-child').on('click', function (e) {
            e.preventDefault();
            $('#block-theme-baluarte-main-menu').removeClass('show');
         });

         /**
          * Quitando margin-bottom en banner superior si no
          * hay banner.
          */
        setTimeout(() => {
            $('.no-zone-1 ins a').length > 0 ? $('.no-zone-1').removeClass('d-none') : console.log('no lo tomó'); ;
        }, 1400);
        
    });

}(jQuery))