<?php

namespace Drupal\floating_wpp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Floating Whatsapp settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'floating_wpp_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['floating_wpp.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['wpp_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Número celular'),
      '#description' => $this->t('Agrege el número celular al cuál se contactarán'),
      '#default_value' => $this->config('floating_wpp.settings')->get('wpp_number'),
    ];
    $form['wpp_msg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mensaje'),
      '#description' => $this->t('Este mensaje se mostrará al inicar la conversación en Whatsapp.'),
      '#default_value' => $this->config('floating_wpp.settings')->get('wpp_msg'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('wpp_number');
    if (strlen($value) < 10) {
      $form_state->setErrorByName('wpp_number', $this->t('La cantidad de dígitos no es correcto.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('floating_wpp.settings')
      ->set('wpp_number', $form_state->getValue('wpp_number'))
      ->set('wpp_msg', $form_state->getValue('wpp_msg'))
      ->save();

      parent::submitForm($form, $form_state);
  }

}
