(function ($, drupalSettings) {

  $(document).ready(function () {

    var number = drupalSettings.floating_wpp.number,
        msg = drupalSettings.floating_wpp.msg;

    msg == null || msg == '' ? msg = 'Hola, estoy interesado/a en alguno de sus servicios' : null ;
    if (number != null){
      var link = '<a href="http://api.whatsapp.com/send?phone=57' + number + '&text='+ msg +'" target="_blank" title="Whatsapp"><img class="img-fluid" src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/whatsapp.svg" /></a>';
      $('#WAButton').append(link);
    }

  });
  
})(jQuery, drupalSettings);