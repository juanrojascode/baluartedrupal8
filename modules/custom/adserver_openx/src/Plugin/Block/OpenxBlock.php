<?php

namespace Drupal\adserver_openx\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'OpenX' Block.
 *
 * @Block(
 *   id = "OpenX",
 *   admin_label = @Translation("Openx Block"),
 * )
 */
class OpenxBlock extends BlockBase {

	public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['data_zone_id'] = array(
      '#type' => 'number',
      '#title' => $this->t('Zone id'),
      '#description' => $this->t('Zone id'),
      '#default_value' => isset($config['data_zone_id']) ? $config['data_zone_id'] : '',
    );

    $form['data_revive_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Revive id'),
      '#description' => $this->t('Revive id'),
      '#default_value' => isset($config['data_revive_id']) ? $config['data_revive_id'] : '',
    );

    $form['class'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Class'),
      '#description' => $this->t('Agregue clases a este bloque'),
      '#default_value' => isset($config['class']) ? $config['class'] : '',
    );

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $data_zone_id = $form_state->getValue('data_zone_id');
    $this->configuration['data_zone_id'] = $data_zone_id;

    $data_revive_id = $form_state->getValue('data_revive_id');
    $this->configuration['data_revive_id'] = $data_revive_id;

    $class = $form_state->getValue('class');
    $this->configuration['class'] = $class;
  }
  /**
   * {@inheritdoc}
   */

  public function build() {
    $config = $this->getConfiguration();
    $numZone='0';
    $class = isset($config['class']) ? $config['class'] : '' ;
    if(is_numeric($config['data_zone_id']) && isset($config['data_revive_id'])){
      $numZone=$config['data_zone_id'];
      $this->return = '<ins data-revive-zoneid="'.$numZone.'" data-revive-id="'.$config['data_revive_id'].'"></ins>';
    }else{
      $this->return = '';
    }
    return array(
        '#type' => 'markup',
        '#markup' => $this->return,
        '#attributes' => array(
          'class' => array(
            "no-zone-".$numZone,
            $class,
          ),
        ),
    );
  }
}